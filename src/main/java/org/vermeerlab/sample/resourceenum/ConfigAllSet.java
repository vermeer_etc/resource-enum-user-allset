/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.sample.resourceenum;

import java.util.Locale;
import java.util.ResourceBundle.Control;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceControl;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.EnumResourceLocale;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.GenerateEnumFromPropertyFile;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeyPrefix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyKeySuffix;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithKey;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.PropertyValueWithValueWhenException;
import org.vermeerlab.apt.command.propertyfileenum.annotaion.TargetPropertyFile;

/**
 *
 * @author Yamashita,Takahiro
 */
@GenerateEnumFromPropertyFile(basePackageName = "base", subPackageName = "sub")
public class ConfigAllSet {

    @TargetPropertyFile(classCommentTitle = "commentHeader", className = "Config", classNamePrefix = "Prefix", classNameSuffix = "Suffix")
    @PropertyValueWithKey(true)
    @PropertyKeyPrefix("【")
    @PropertyKeySuffix("】")
    @PropertyValueWithValueWhenException(true)
    final String resourceName = "message";

    @EnumResourceLocale
    final Locale locale() {
        return Locale.ENGLISH;
    }

    @EnumResourceControl
    final Control control() {
        return java.util.ResourceBundle.Control.getNoFallbackControl(
                java.util.ResourceBundle.Control.FORMAT_DEFAULT);
    }

}
