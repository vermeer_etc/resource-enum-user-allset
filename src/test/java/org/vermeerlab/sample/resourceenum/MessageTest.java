/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.sample.resourceenum;

import base.sub.PrefixConfigSuffix;
import java.util.Locale;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class MessageTest {

    public MessageTest() {
    }

    @Test
    public void messageToString() {
        Assert.assertThat(PrefixConfigSuffix.MSG001.toString(), is("【msg001】message001"));
    }

    @Test
    public void messageFormatEnglish() {
        Assert.assertThat(PrefixConfigSuffix.MSG001.format(Locale.JAPAN), is("【msg001】メッセージ００１"));
    }

    @Test
    public void messageFormatFallback() {
        Assert.assertThat(PrefixConfigSuffix.MSG001.format(Locale.ITALIAN), is("【msg001】ルートメッセージ００１"));
    }

}
